
# Functions
## Infrastructure:
* Provide hardware, VMs, Kubernetes cluster
* Provide basic services such as mail routing, mailman HTTPS routing, auth, CI/CD, RT, wiki

## R&D:
* Long-term dev - looking for or developing alternatives to existing solutions
	* Eg - replace symposion's Proposals with a standalone multi-year solution

## Operations - Dev/Deployment:
* Short-term/incremental dev efforts
	* Eg - pull together LCA2018, LCA2017, PyCon2017, NBayPycon, Upstream code
	* Add/modify features as required for a particular conference
	* Maintenance and upgrades
* Deployment and support of solution for conference teams

## Conference Management:
* Admin of conference management solution (Symposion or equivalent) - setting up inventory, CFPs, scheduling etc
* Content management for "Static" site, wiki
* Theming/design

# Teams:
##LA Admin Team:
* Provide Hardware/VMs?
* Provide RT, Mailman, mail routing
* Provide Kubernetes?

## Developer team:
* Dev experience enhancements
	* Describe conference lifecycle
	* Create sample data and tooling to make it simple to set up a sympiola instance with useful test data at any point of the conference workflow
* Pull together a unified LA fork/distribution of symposion+registrasion+regidesk+registripe (call it symposiola)
* Enhance/extend symposiola
* Enhance/extend documentation for Conference Mgmt team

## Conference Infra Team:
* Extend on what can be provided by LA Admin team - eg, if LA Admin team can't provide kubernetes, find a commercial vendor

## Conference Team:
* Branding, design, website content
* Timelines

